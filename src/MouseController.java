import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.InputEvent;
import java.util.List;

public class MouseController {
    private Robot robot;
    private Rectangle window;

    public MouseController(Rectangle rect) throws AWTException {
        robot = new Robot();
        window = rect;
    }

    public void clickLocation(Point location) {
        Agent.getActiveWindowRect();
        robot.mouseMove(window.x + location.x, window.y + location.y);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    public void rightClickMultipleLocations(List<Point> locations) {
        Agent.getActiveWindowRect();
        for (Point location: locations) {
            robot.mouseMove(window.x + location.x, window.y + location.y);
            robot.mousePress(InputEvent.BUTTON3_MASK);
            robot.mouseRelease(InputEvent.BUTTON3_MASK);
        }
    }

    public void leftClickMultipleLocations(List<Point> locations) {
        Agent.getActiveWindowRect();
        for (Point location: locations) {
            robot.mouseMove(window.x + location.x, window.y + location.y);
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
        }
    }

    // If you beat the highscore a window pops up to congratulate you. We want to close that window.
    public void closeCongratsWindow() {
        final User32 u32 = User32.INSTANCE;
        WinDef.HWND hwnd = u32.FindWindow(null, "Congratulations");
        if(hwnd != null){
            WinDef.RECT rectangle = new WinDef.RECT();
            u32.GetWindowRect(hwnd, rectangle);
            Rectangle r = rectangle.toRectangle();
            robot.mouseMove(r.x + (r.width - 20), r.y + (r.height - 20));
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            robot.delay(1500);
            this.closeHighScore();
        }
    }

    // After you close the congratulation window a highscore window pops up that we need to close
    private void closeHighScore() {
        final User32 u32 = User32.INSTANCE;
        WinDef.HWND hwnd = u32.FindWindow(null, "Best Times");
        if(hwnd != null){
            WinDef.RECT rectangle = new WinDef.RECT();
            u32.GetWindowRect(hwnd, rectangle);
            Rectangle r = rectangle.toRectangle();
            robot.mouseMove(r.x + (r.width - 20), r.y + (r.height - 20));
            robot.mousePress(InputEvent.BUTTON1_MASK);
            robot.mouseRelease(InputEvent.BUTTON1_MASK);
            robot.delay(1500);
        }
    }
}
