import com.sun.jna.platform.win32.User32;
import com.sun.jna.platform.win32.WinDef;

import java.awt.*;
import java.awt.event.InputEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class Agent {

    private MouseController mouseController;
    private Minefield minefield;
    private TankSearch tankSearch;
    private Rectangle fullActiveWindow;

    public Agent() throws AWTException {
        Rectangle activeWindow = getActiveWindowRect();
        fullActiveWindow = activeWindow;
        activeWindow = ScreenCapture.setScreenShotWindow(activeWindow);
        mouseController = new MouseController(activeWindow);
        tankSearch = new TankSearch();
    }

    public void start() throws AWTException, IOException {
        minefield = ScreenCapture.getMinefield();
        int howMany = 0;
        int maxGames = 100;
        int won = 0;
        int lost = 0;

        while(howMany != maxGames) {
            mouseController.clickLocation(getFirstMove());
            while (true) {
                if(ScreenCapture.gameIsLost()){
                    lost++;
                    break;
                }else if(ScreenCapture.gameIsWon()){
                    won++;
                    mouseController.closeCongratsWindow();
                    break;
                }
                nextMove();
            }
            reset();
            howMany++;
        }
        System.out.println("Agent played " + maxGames + " Won: " + won + " Lost: " + lost);
    }

    private void reset() throws AWTException {
        Robot robot = new Robot();
        robot.mouseMove(fullActiveWindow.x + (fullActiveWindow.width / 2), fullActiveWindow.y + 65);
        robot.mousePress(InputEvent.BUTTON1_MASK);
        robot.mouseRelease(InputEvent.BUTTON1_MASK);
    }

    private void nextMove() throws AWTException, IOException {
        minefield = ScreenCapture.getMinefield();
        ArrayList<Point> safeFlags = minefield.getSafeFlags();
        mouseController.rightClickMultipleLocations(safeFlags);
        minefield = ScreenCapture.getMinefield();
        ArrayList<Point> safeMoves = minefield.getSafeMoves();
        mouseController.leftClickMultipleLocations(safeMoves);
        if(safeFlags.isEmpty() && safeMoves.isEmpty()){
            ArrayList<Square> allBorderSquares = minefield.getAllSquaresWithCoveredNeighbours();
            if(allBorderSquares.size() != 0) {
                Square move = tankSearch.findSafestMove(minefield, allBorderSquares);
                mouseController.clickLocation(move.position);
            }else{
                Square move = minefield.getRandomCoveredSquare();
                mouseController.clickLocation(move.position);
            }
        }
    }

    private Point getFirstMove(){
        Random r = new Random();
        return minefield.getSquarePosition(r.nextInt(minefield.rows - 2) + 1, r.nextInt(minefield.cols - 2) + 1);
    }

    public static Rectangle getActiveWindowRect() {
        final User32 u32 = User32.INSTANCE;
        WinDef.HWND hwnd = u32.FindWindow(null, "Minesweeper X");
        if(hwnd == null){
            System.err.println("No Minesweeper X window found!");
            System.exit(1);
        }
        u32.ShowWindow(hwnd, 9);
        u32.SetForegroundWindow(hwnd);
        WinDef.RECT rectangle = new WinDef.RECT();
        u32.GetWindowRect(hwnd, rectangle);
        return rectangle.toRectangle();
    }
}
