import java.awt.*;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class main {

    public static void main(String[] args) {
        try {
            /* Give the program few seconds to start
                so it wont focus the MineSweeper window instantly
              */
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // Start the agent
        Agent agent = null;
        try {
            agent = new Agent();
        } catch (AWTException e) {
            e.printStackTrace();
        }
        try {
            agent.start();
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
