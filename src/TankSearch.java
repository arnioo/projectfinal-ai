import java.util.*;

public class TankSearch {

    private Minefield minefield;
    private ArrayList<Square> squares = new ArrayList<>();
    private ArrayList<Node> leafs;

    public Square findSafestMove(Minefield minefield, ArrayList<Square> squares){
        this.minefield = new Minefield(minefield);
        this.squares = new ArrayList<>(squares);
        ArrayList<ArrayList<Square>> disjointedSections = minefield.getDisjointedSections(squares);
        Square champion = null;
        leafs = new ArrayList<>();
        Minefield m = new Minefield(minefield);
        for(ArrayList<Square> list : disjointedSections) {
            Node initNode = new Node(minefield, null, list.get(0), 0, new ArrayList<>());
            bfs(initNode, list);
        }
        setTankValues(m);
        return minimumTankValueSquare(m);
    }

    private void bfs(Node init, ArrayList<Square> list){
        Queue<Node> frontierQueue = new LinkedList<>();
        ArrayList<Square> currentSection = list;
        frontierQueue.add(init);
        
        while(!frontierQueue.isEmpty()){
            Node current = frontierQueue.remove();
            if(current.square == null){
                int counter = 0;
                for(Square square : currentSection){
                    if(!current.minefield.checkIfValid(square)){
                        counter++;
                    }
                }
                if(counter == 0) {
                    leafs.add(current);
                }
                continue;
            }
            Square nextUp = null;
            if(list.size() > current.level + 1) {
                nextUp = list.get(current.level + 1);
            }
            int flagsToSet = current.square.value - current.minefield.getNumSurroundingFlagged(current.square);
            if(flagsToSet <= 0){
                if(nextUp != null) {
                    Node child = new Node(new Minefield(current.minefield), current, nextUp, current.level + 1, current.flagged);
                    frontierQueue.add(child);
                }
                else{
                    leafs.add(current);
                }
                continue;
            }
            ArrayList<Square> moves = current.minefield.getPossibleFlaggableSquares(current.square, flagsToSet);
            for(int i = 0; i < moves.size(); i = i + flagsToSet){
                Minefield field = new Minefield(current.minefield);
                for(int j = 0; j < flagsToSet; j++){
                    field.squares[moves.get(i+j).y][moves.get(i+j).x].flagged = true;
                }
                field.lockRemainingSurroundingCoveredSquares(current.square);
                Node child = new Node(field, current, nextUp, current.level + 1, current.flagged);
                for(int k = 0; k < flagsToSet; k++) {
                    child.flagged.add(moves.get(i+k));
                }
                frontierQueue.add(child);
            }
            if(moves.size() == 0 && nextUp != null){
                frontierQueue.add(new Node(current.minefield, current, nextUp, current.level + 1, current.flagged));
            }
        }
    }

    private void setTankValues(Minefield m){
        int flagsLeft = m.getMaxFlags() - m.getNumFlags();
        for(Node node : leafs){
            if(m.getMaxFlags() - node.minefield.getNumFlags() > flagsLeft){
                continue;
            }
            for(Square s : node.flagged){
                m.incrementTankValue(s.x, s.y);
            }
        }
    }

    private Square minimumTankValueSquare(Minefield m){
        Random r = new Random();
        int value = Integer.MAX_VALUE;
        ArrayList<Square> champions = new ArrayList<>();
        for(Node node : leafs){
            for(Square s : node.flagged){
                if(m.squares[s.y][s.x].tankValue < value){
                    value = m.squares[s.y][s.x].tankValue;
                    champions.clear();
                    champions.add(m.squares[s.y][s.x]);
                }
                else if(m.squares[s.y][s.x].tankValue == value){
                    champions.add(m.squares[s.y][s.x]);
                }
            }
        }
        return champions.get(r.nextInt(champions.size()));
    }
}