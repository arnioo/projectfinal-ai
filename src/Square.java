public class Square {
    public boolean flagged;
    public boolean covered;
    public int value;
    public Point position;
    public int x;
    public int y;

    // Used for tank solver algorithm
    public int tankValue;
    public boolean locked;

    public Square(int x, int y) {
        flagged = false;
        covered = true;
        value = 0;
        position = new Point(0, 0);
        tankValue = -1;
        this.x = x;
        this.y = y;
        locked = false;
    }

    public Square(Square s){
        this.flagged = s.flagged;
        this.covered = s.covered;
        this.value = s.value;
        this.position = new Point(s.position.x, s.position.y);
        this.tankValue = s.tankValue;
        this.x = s.x;
        this.y = s.y;
        this.locked = s.locked;
    }

    @Override
    public String toString(){
        return x + ", " + y + " : TankValue: " + tankValue;
    }
}
