/**
 * Combination Path Node
 */
public class CPNode {
    public CPNode parent;
    public Square current;

    public CPNode(CPNode parent, Square current){
        this.parent = parent;
        this.current = current;
    }
}
