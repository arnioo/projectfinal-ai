import java.util.ArrayList;

public class Node {
    public Minefield minefield;
    public Node parent;
    public Square square;
    public int level;
    public ArrayList<Square> flagged;

    public Node(Minefield minefield, Node parent, Square square, int level, ArrayList<Square> flagged){
        this.minefield = new Minefield(minefield);
        this.parent = parent;
        this.square = square;
        this.level = level;
        this.flagged = new ArrayList<>(flagged);
    }
}
