import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public final class ScreenCapture {

    private static final int ONE = new Color(0, 0, 255).getRGB();
    private static final int TWO = new Color(0, 128, 0).getRGB();
    private static final int THREE = new Color(255, 0, 0).getRGB();
    private static final int FOUR = new Color(0, 0, 128).getRGB();
    private static final int FIVE = new Color(128, 0, 0).getRGB();
    private static final int SIX = new Color(0, 128, 128).getRGB();
    private static final int SEVEN = new Color(0, 0, 0).getRGB();
    private static final int EIGHT = new Color(128, 128, 128).getRGB();
    private static final int EMPTY = new Color(192, 192, 192).getRGB();
    private static final int WHITE = new Color(255, 255, 255).getRGB();
    private static final int RED = THREE;
    private static final int BLACK = SEVEN;

    private static Rectangle window;
    private static Rectangle face;
    //1: for flag, 2: for uncovered or loss, 3: for number
    private static Point[] pointsToCheck = {new Point(11, 11), new Point(1, 1), new Point(9, 8)};

    public static Rectangle setScreenShotWindow(Rectangle rect) {
        /*
        Beginner: 158 x 244
        Intermediate: 286 x 372
        Expert: 510 x 372
         */

        window = new Rectangle(rect.x + 15, rect.y + 101, rect.width - 30, rect.height - 116);
        face = new Rectangle(rect.x + (rect.width / 2) - 9, rect.y + 65, 19, 19);
        return window;
    }

    public static Minefield getMinefield() throws AWTException, IOException {
        Robot robot = new Robot();
        BufferedImage image = robot.createScreenCapture(window);
        return parseImageToMinefield(image);
    }

    /* When the game has been won the face in the scoreboard is wearing sunglasses which are black and 9px long,
         gameIsWon() iterates through those pixels and check if they're there.
     */
    public static boolean gameIsWon() throws AWTException {
        Robot robot = new Robot();
        BufferedImage image = robot.createScreenCapture(face);
        for(int i = 5; i < 14; i++) {
            if(image.getRGB(i, 6) != BLACK){
                return false;
            }
        }
        return true;
    }

    public static boolean gameIsLost() throws AWTException {
        Robot robot = new Robot();
        BufferedImage image = robot.createScreenCapture(face);
        image.getRGB(6, 6);
        return image.getRGB(5, 5) == BLACK && image.getRGB(5, 7) == BLACK &&
                image.getRGB(6, 6) == BLACK && image.getRGB(7, 5) == BLACK
                && image.getRGB(7, 7) == BLACK;
    }

    private static Minefield parseImageToMinefield(BufferedImage image) {
        // Divided by 16 because each cell is 16x16 pixels
        int height = image.getHeight() / 16;
        int width = image.getWidth() / 16;
        Minefield result = new Minefield(height, width);
        Point offset = new Point(0, 0);
        for(int i = 0; i < height; i++) {
            for(int j = 0; j < width; j++) {
                offset.y = 16 * i;
                offset.x = 16 * j;
                // Set the position of the square in the middle of the square, to make it easier to click later
                result.squares[i][j].position.x = offset.x + 8;
                result.squares[i][j].position.y = offset.y + 8;
                // First check if square is flagged
                Point checkPoint = pointsToCheck[0].add(offset);
                if(image.getRGB(checkPoint.x, checkPoint.y) == BLACK) {
                    result.squares[i][j].flagged = true;
                    continue;
                }
                // Second we check if the square is covered
                checkPoint = pointsToCheck[1].add(offset);
                if(image.getRGB(checkPoint.x, checkPoint.y) == WHITE) {
                    result.squares[i][j].covered = true;
                    continue;
                }
                // Lastly we check the number value in the square
                checkPoint = pointsToCheck[2].add(offset);
                result.squares[i][j].covered = false;
                result.squares[i][j].value = getSquareValue(image.getRGB(checkPoint.x, checkPoint.y));
            }
        }
        return result;
    }

    private static int getSquareValue(int color){
        if(color == ONE){
            return 1;
        }
        else if(color == TWO){
            return 2;
        }
        else if(color == THREE){
            return 3;
        }
        else if(color == FOUR){
            return 4;
        }
        else if(color == FIVE){
            return 5;
        }
        else if(color == SIX){
            return 6;
        }
        else if(color == SEVEN){
            return 7;
        }
        else if(color == EIGHT){
            return 8;
        }
        else{
            return 0;
        }
    }
}
