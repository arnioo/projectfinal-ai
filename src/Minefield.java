import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;

public class Minefield {
    public Square[][] squares;
    public int rows;
    public int cols;

    public Minefield(int rows, int cols) {
        this.rows = rows;
        this.cols = cols;
        squares = new Square[rows][cols];
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                squares[i][j] = new Square(j, i);
            }
        }
    }

    public Minefield(Minefield m){
        rows = m.rows;
        cols = m.cols;
        squares = new Square[rows][cols];
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                squares[i][j] = new Square(m.squares[i][j]);
            }
        }
    }

    public void setFlagged(int x, int y){
        squares[y][x].flagged = true;
    }

    public ArrayList<Point> getSafeMoves(){
        ArrayList<Point> result = new ArrayList<>();
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(!squares[i][j].covered && squares[i][j].value != 0){
                    List<Square> temp = getSurroundingCovered(i, j);
                    int count = 0;
                    for(Square square : temp){
                        if(square.flagged){
                            count++;
                        }
                    }
                    if(count == squares[i][j].value) {
                        for(Square square : temp){
                            if(!square.flagged && !result.contains(square.position)){
                                result.add(square.position);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    public ArrayList<Point> getSafeFlags(){
        ArrayList<Point> result = new ArrayList<>();
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(!squares[i][j].covered && squares[i][j].value != 0){
                    List<Square> temp = getSurroundingCovered(i, j);
                    if(temp.size() == squares[i][j].value) {
                        for(Square square : temp){
                            if(!square.flagged && !result.contains(square.position)){
                                result.add(square.position);
                            }
                        }
                    }
                }
            }
        }
        return result;
    }

    private ArrayList<Square> getSurroundingCovered(int row, int col){
        ArrayList<Square> result = new ArrayList();
        for(int i = -1; i < 2; i++){
            for(int j = -1; j < 2; j++){
                if(i == 0 && j == 0){
                    continue;
                }
                if(!(row + i >= 0 && row + i < rows)){
                    continue;
                }
                if(!(col + j >= 0 && col + j < cols)){
                    continue;
                }
                if(squares[row + i][col + j].covered){
                    result.add(squares[row + i][col + j]);
                }
            }
        }
        return result;
    }

    public ArrayList<Square> getPossibleFlaggableSquares(Square s, int flagsToSet){
        ArrayList<Square> result = new ArrayList<>();
        ArrayList<Square> temp = new ArrayList<>();
        for(Square square : getSurroundingCovered(s.y, s.x)){
            if(!square.flagged && !square.locked){
                temp.add(square);
            }
        }
        getAllPossibleFlagCombinations(result, temp, flagsToSet, null);
        return result;
    }

    private void getAllPossibleFlagCombinations(ArrayList<Square> result, ArrayList<Square> squareList, int flagsToSet, CPNode parent){
        for (int i = 0; i <= squareList.size() - flagsToSet; i++){
            CPNode current = new CPNode(parent, squareList.get(i));
            if(flagsToSet > 1){
                ArrayList<Square> temp = new ArrayList<>(squareList.subList(i + 1, squareList.size()));
                getAllPossibleFlagCombinations(result, temp, flagsToSet - 1, current);
            }
            else{
                Stack<Square> path = new Stack<>();
                while(current != null) {
                    path.push(current.current);
                    current = current.parent;
                }
                while(!path.isEmpty()){
                    result.add(path.pop());
                }
            }
        }
    }

    public ArrayList<Square> getAllCoveredSquares(){
        ArrayList<Square> result = new ArrayList();
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                if(squares[i][j].covered && !squares[i][j].flagged){
                    result.add(squares[i][j]);
                }
            }
        }
        return result;
    }

    public ArrayList<Square> getAllSquaresWithCoveredNeighbours(){
        ArrayList<Square> result = new ArrayList();
        for(int i = 0; i < rows; i++) {
            for(int j = 0; j < cols; j++) {
                if(squares[i][j].covered){
                    continue;
                }
                ArrayList<Square> temp = getSurroundingCovered(i, j);
                if(temp.isEmpty()){
                    continue;
                }
                int counter = 0;
                for(Square s: temp){
                    if(!s.flagged){
                        counter++;
                    }
                }
                if(counter == 0){
                    continue;
                }
                result.add(squares[i][j]);
            }
        }
        return result;
    }

    public int getNumSurroundingFlagged(Square s){
        int result = 0;
        ArrayList<Square> temp = getSurroundingCovered(s.y, s.x);
        for(Square square : temp){
            if(square.flagged){
                result++;
            }
        }
        return result;
    }

    public void lockRemainingSurroundingCoveredSquares(Square s){
        ArrayList<Square> temp = getSurroundingCovered(s.y, s.x);
        for(Square square : temp){
            if(!square.flagged){
                square.locked = true;
            }
        }
    }

    public ArrayList<ArrayList<Square>> getDisjointedSections(ArrayList<Square> squares){
        ArrayList<Square> possibleSquares = new ArrayList<>(squares);
        ArrayList<ArrayList<Square>> result = new ArrayList<>();
        ArrayList<Square> path = new ArrayList<>();
        while(!possibleSquares.isEmpty()){
            path.add(possibleSquares.get(0));
            int addedCounter = 1; // set as 1 so we will execute the while loop
            int currentPathSize;
            while(addedCounter > 0) {
                addedCounter = 0;
                currentPathSize = path.size();
                for(int j = 0; j < currentPathSize; j++){
                    for(int i = 1; i < possibleSquares.size(); i++){
                        if(path.contains(possibleSquares.get(i))){
                            continue;
                        }
                        if(checkIfConnected(path.get(j), possibleSquares.get(i))){
                            path.add(possibleSquares.get(i));
                            addedCounter++;
                        }
                    }
                }
            }
            result.add(path);
            for(Square square : path){
                possibleSquares.remove(square);
            }
            path = new ArrayList<>();
        }
        return result;
    }

    private boolean checkIfConnected(Square s1, Square s2){
        ArrayList<Square> s1Surrounding = new ArrayList<>();
        for(Square a : getSurroundingCovered(s1.y, s1.x)){
            if(!a.flagged){
                s1Surrounding.add(a);
            }
        }
        ArrayList<Square> s2Surrounding = new ArrayList<>();
        for(Square b : getSurroundingCovered(s2.y, s2.x)){
            if(!b.flagged){
                s2Surrounding.add(b);
            }
        }
        for(Square c : s1Surrounding){
            if(s2Surrounding.contains(c)){
                return true;
            }
        }
        return false;
    }

    public boolean checkIfValid(Square s){
        ArrayList<Square> list = getSurroundingCovered(s.y, s.x);
        int counter = 0;
        for(Square square : list){
            if(square.flagged){
                counter++;
            }
        }
        if(counter == s.value){
            return true;
        }
        else{
            return false;
        }
    }

    public Square getRandomCoveredSquare(){
        Random r = new Random();
        ArrayList<Square> list = getAllCoveredSquares();
        if(list.size() == 1){
            return list.get(0);
        }
        else {
            return list.get(r.nextInt(list.size() - 1));
        }
    }

    public int getNumFlags(){
        int result = 0;
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                if(squares[i][j].flagged){
                    result++;
                }
            }
        }
        return result;
    }

    public int getMaxFlags(){
        if(cols == 8){
            return 10;
        }
        else if(cols == 16){
            return 40;
        }
        else {
            return 99;
        }
    }

    public void incrementTankValue(int x, int y){
        squares[y][x].tankValue++;
    }

    public Point getSquarePosition(int row, int col){
        return squares[row][col].position;
    }

    @Override
    public String toString(){
        String result = "";
        for(int i = 0; i < rows; i ++){
            result += "|";
            for(int j = 0; j < cols; j++){
                if(squares[i][j].flagged){
                    result += "f|";
                }
                else if(squares[i][j].covered){
                    result += "c|";
                }
                else if(squares[i][j].value == 0){
                    result += " |";
                }
                else{
                    result += squares[i][j].value + "|";
                }
            }
            result += "\n";
        }
        return result;
    }
}
